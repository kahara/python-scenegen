"""Package level tests"""
from scenegen import __version__
from scenegen.scene import Scene


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "0.1.2"


def test_scene() -> None:
    """Test scene functionality"""
    scene = Scene()
    assert scene.width == 1024
    assert scene.height == 768
    assert scene.description == {}
