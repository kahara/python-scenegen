"""Tests for scene walker"""
# pylint: disable=W0621

from typing import cast, Any, Dict

import pytest  # type: ignore

from scenegen.walk import walk


@pytest.fixture
def hollow_static_scene():  # type: ignore
    """A hollow static scene description"""
    return {
        "bool": True,
        "int": 1,
        "float": 1.0,
        "str": "word",
    }


@pytest.fixture
def deep_static_scene(hollow_static_scene):  # type: ignore
    """A deep static scene description"""
    return dict(hollow_static_scene, **{"child": hollow_static_scene})


@pytest.fixture
def dynamic_return_value(hollow_static_scene):  # type: ignore
    """A value returned from callables"""

    def _dynamic_return_value():  # type: ignore
        """Return a value"""
        return hollow_static_scene

    return _dynamic_return_value


@pytest.fixture
def hollow_dynamic_scene(hollow_static_scene, dynamic_return_value):  # type: ignore
    """A hollow dynamic scene description"""

    return dict(hollow_static_scene, **{"callable": dynamic_return_value})


@pytest.fixture
def deep_dynamic_scene(hollow_static_scene, dynamic_return_value):  # type: ignore
    """A deep dynamic scene description"""

    def fnct():  # type: ignore
        """Produce a value"""
        return dynamic_return_value

    return dict(hollow_static_scene, **{"callable": fnct})


@pytest.fixture
def hollow_dynamic_scene_flattened(hollow_dynamic_scene):  # type: ignore
    """A flattened hollow dynamic scene"""
    hollow_dynamic_scene["callable"] = hollow_dynamic_scene["callable"]()
    return hollow_dynamic_scene


@pytest.fixture
def deep_dynamic_scene_flattened(deep_dynamic_scene):  # type: ignore
    """A flattened deep dynamic scene"""
    deep_dynamic_scene["callable"] = deep_dynamic_scene["callable"]()()
    return deep_dynamic_scene


def test_walk_type_basic():  # type: ignore
    """Test basic types are returned as-is"""
    with pytest.raises(ValueError):
        walk(0, None)

    for basic_type in (True, 1, 1.0, "word", {"foo": "bar"}):
        assert walk(0, basic_type) == basic_type


def test_walk_type_callable():  # type: ignore
    """Test callables are called"""

    def fnct(clock: int) -> int:  # pylint: disable=W0613
        """A dummy function"""
        return 1

    assert walk(0, fnct) == fnct(0)


def test_walk_empty():  # type: ignore
    """Test walking an empty scene"""
    scene: Dict[Any, Any] = {}
    assert walk(0, scene) == scene


def test_walk_static_hollow(hollow_static_scene):  # type: ignore
    """Test walking a hollow static scene"""
    assert walk(0, hollow_static_scene) == hollow_static_scene


def test_walk_static_deep(deep_static_scene):  # type: ignore
    """Test walking a deep static scene"""
    assert walk(0, deep_static_scene) == deep_static_scene


def test_walk_dynamic_hollow(hollow_dynamic_scene, hollow_dynamic_scene_flattened):  # type: ignore
    """Test walking a hollow dynamic scene"""
    assert walk(0, hollow_dynamic_scene) == hollow_dynamic_scene_flattened


def test_walk_dynamic_deep(deep_dynamic_scene, deep_dynamic_scene_flattened):  # type: ignore
    """Test walking a deep dynamic scene"""
    assert walk(0, deep_dynamic_scene) == deep_dynamic_scene_flattened


def test_walk_clock_simple():  # type: ignore
    """Test that clock works"""
    scene = {
        "fnct": (lambda clock: clock ** 2),
    }
    for clock in (0, 1, 2, 5, 10):
        assert cast(Dict[str, int], walk(clock, scene))["fnct"] == clock ** 2
