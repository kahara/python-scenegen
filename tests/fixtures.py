"""Scenegen test fixtures"""
# pylint: disable=W0611

from .scene_fixtures import scene_fadeinout, scene_colorcircles
